# Tuxli install scripts

## Arch linux

### Features
* German language
* KDE Plasma (optional)
* Xpra (optional)

### Preconditions
* Create partitions
* Mounted destination filesystem

### Installation
```
wget http://install.tuxli.de/arch.sh
bash arch.sh
```

### Post steps
Install grub to disk:
```
arch-chroot [MOUNT_DIR] grub-install /dev/...
```
