#!/bin/bash
set -e

echo "#####################################"
echo "Welcome to Tuxli ArchLinux installer!"
echo "#####################################"
echo

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# Definitions
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #


DEFAULT_HOSTNAME="arch"
DEFAULT_MOUNT_DIR="/mnt/"


BASE_APPS="linux base base-devel openssh vim bash-completion rsync wget curl reflector nfs-utils ntfs-3g acpid ntp networkmanager" 
BASE_AUR=""
BOOTLOADER_GRUB="grub"
BOOTLOADER_SYSTEMD="efibootmgr"

DESKTOP_BASE_APPS="pulseaudio pulseaudio-alsa xorg xorg-xinit sudo ttf-dejavu ttf-liberation"
DESKTOP_MORE_APPS="okular vlc kdiff3 tilix"
DESKTOP_FONTS="ttf-dejavu ttf-droid ttf-freefont ttf-liberation"
DESKTOP_AUR_APPS="google-chrome"

KDE_BASE_APPS="plasma-desktop breeze breeze-gtk kde-gtk-config sddm plasma-nm plasma-pa kscreen dolphin powerdevil"
KDE_MORE_APPS="konsole kdeplasma-addons ark kinfocenter kwalletmanager kwallet-pam gwenview kipi-plugins speedcrunch"
KDE_AUR_APPS=""

ECRYPTFS_APPS="ecryptfs-utils rsync lsof"

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# Functions
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

function read_string {
    local ret_val=$1
    local msg=$2
    local not_empty=$3
    local silent=$4

    local value=""
    until [[ -n "$value" ]]; do
        echo -n "$msg: "

        # check if silent input is requested
        if [[ $silent != 1 ]]; then
            read value
        else
            read -s value
            echo
        fi

        # stop if empty values are possible
        if [[ $not_empty == 1 ]]; then
            break
        fi

        if [[ -z "$value" ]]; then
            echo "[Err] Empty input! Try again"
            echo
        fi
    done
    eval $ret_val="$value";
}
function read_bool {
    local ret_val=$1
    local msg=$2
    local default=$3
    
    eval $ret_val=$default;

    local value=""
    if [[ $default == 0 ]]; then
        echo -n "$msg - yes/no (Default: yes): "
    else
        echo -n "$msg - yes/no (Default: no): "
    fi
    read value

    if [[ -n "$value" ]]; then
        value="${value:0:1}"
        if [[ "$value" == "y" || "$value" == "Y" ]]; then
            eval $ret_val=0;
        else
            eval $ret_val=1;
        fi
    fi
}
function read_password {
    local ret_val=$1

    local pass1=" "
    local pass2=""
    until [[ "$pass1" == "$pass2" ]]; do
        read_string pass1 "Password" 0 1
        read_string pass2 "Retype password" 0 1
        if [[ "$pass1" != "$pass2" ]]; then
            echo "[Err] Passwords does not match! Try again"
            echo
        fi
    done
    eval $ret_val="$pass1";
}

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# Request user input
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

# try to load config file
if [[ -f arch.cfg ]]; then
    . arch.cfg
fi

echo "Root account"
echo "------------------------------"
if [[ -z "$arch_root_password" ]]; then
    read_password arch_root_password
fi
echo

echo "User account"
echo "------------------------------"
if [[ -z "$arch_user_name" ]]; then
    read_string arch_user_name "User name"
fi
if [[ -z "$arch_user_password" ]]; then
    read_password arch_user_password
fi

echo
echo "System settings"
echo "------------------------------"
if [[ -z "$arch_host_name" ]]; then
    read_string arch_host_name "Hostname (Default: \"arch\")" 1
fi
if [[ -z "$arch_host_name" ]]; then
    arch_host_name=$DEFAULT_HOSTNAME
fi
if [[ -z "$arch_mount" ]]; then
    read_string arch_mount "Install destination (Default: \"/mnt/\")" 1
fi
if [[ -z "$arch_mount" ]]; then
    arch_mount=$DEFAULT_MOUNT_DIR
fi
if [[ -z "$arch_mirror" ]]; then
    read_string arch_mirror "Arch mirror cache (Default: \"\")" 1
fi
if [[ -z "$arch_lang_de" ]]; then
    read_bool arch_lang_de "German language" 1
fi

echo
echo "User settings"
echo "------------------------------"
if [[ -z "$encrypt_home" ]]; then
    read_bool encrypt_home "Encrypt $arch_user_name home directory" 1
fi
if [[ -z "$user_dotfiles_repo" ]]; then
    read_string user_dotfiles_repo "Git repository with dotfiles (Default: \"\")" 1
fi


echo
echo "System type"
echo "------------------------------"
if [[ -z "$arch_is_desktop" ]]; then
    read_bool arch_is_desktop "Desktop" 0
fi
echo
echo "Bootloader"
echo "------------------------------"
if [[ -z "$arch_use_grub" ]]; then
    read_bool arch_use_grub "Grub" 0
fi
if [[ -z "$arch_use_systemd" ]]; then
    read_bool arch_use_systemd "systemd" 1
fi

echo "========================================"
echo "Installation information"
echo "========================================"
echo "User name:           $arch_user_name"
echo "Hostname:            $arch_host_name"
echo "Install destination: $arch_mount"
if [[ -n "$arch_mirror" ]]; then
    echo "Arch mirror cache:   $arch_mirror"
else
    echo "Arch mirror cache:   No"
fi
if [[ $arch_lang_de == 0 ]]; then
    echo "German language:     Yes"
else
    echo "German language:     No"
fi
if [[ $encrypt_home == 0 ]]; then
    echo "Encrypt home:        Yes"
else
    echo "Encrypt home:        No"
fi
if [[ -n "$user_dotfiles_repo" ]]; then
    echo "Dotfiles git:        $user_dotfiles_repo"
else
    echo "Dotfiles git:        No"
fi
echo "----------------------------------------"
if [[ $arch_is_desktop == 0 ]]; then
    echo "Desktop:   Yes"
else
    echo "Desktop:   No"
fi
echo "========================================"
if [[ $arch_use_grub == 0 ]]; then
    echo "Bootloader:  Grub"
elif [[ $arch_use_systemd == 0 ]]; then
    echo "Bootloader:  systemd"
else
    echo "Bootloader:  None"
fi
echo "========================================"

echo
read_bool _start_install "Start installation" 0
if [[ $_start_install == 1 ]]; then
    echo "Installation stopped!"
    exit 1
fi

# build installation variables
MOUNT_DIR=$arch_mount
PACSTRAP_PACKAGES="$BASE_APPS"
AUR_PACKAGES="$BASE_AUR"

if [[ $arch_use_grub == 0 ]]; then
    PACSTRAP_PACKAGES="$PACSTRAP_PACKAGES $BOOTLOADER_GRUB"
elif [[ $arch_use_systemd == 0 ]]; then
    PACSTRAP_PACKAGES="$PACSTRAP_PACKAGES $BOOTLOADER_SYSTEMD"
fi

if [[ $arch_is_desktop == 0 ]]; then
    PACSTRAP_PACKAGES="$PACSTRAP_PACKAGES $DESKTOP_BASE_APPS $DESKTOP_MORE_APPS $DESKTOP_FONTS"
    PACSTRAP_PACKAGES="$PACSTRAP_PACKAGES $KDE_BASE_APPS $KDE_MORE_APPS"
    AUR_PACKAGES="$AUR_PACKAGES $DESKTOP_AUR_APPS $KDE_AUR_APPS"
fi

if [[ $encrypt_home == 0 ]]; then
    PACSTRAP_PACKAGES="$PACSTRAP_PACKAGES $ECRYPTFS_APPS"
fi

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# Installation
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# >> BASE <<
# Note: This requires prepared and mounted partitions at $MOUNT_DIR
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
echo ">>Prepare base system"

# arch mirror cache
if [[ -n "$arch_mirror" && ! -f /etc/pacman.d/archmirror ]]; then
    echo "Server = http://$arch_mirror/\$repo/os/\$arch" > /etc/pacman.d/archmirror

    sed 's:\[core\]:\[core\]\nInclude = /etc/pacman.d/archmirror:g' -i /etc/pacman.conf
    sed 's:\[extra\]:\[extra\]\nInclude = /etc/pacman.d/archmirror:g' -i /etc/pacman.conf
    sed 's:\[community\]:\[community\]\nInclude = /etc/pacman.d/archmirror:g' -i /etc/pacman.conf
    sed 's:\[multilib\]:\[multilib\]\n#Include = /etc/pacman.d/archmirror:g' -i /etc/pacman.conf
fi

# install helper
pacman -Sy --noconfirm reflector curl

# update mirror list
reflector -c "Germany" -f 12 -l 10 -n 12 --save /etc/pacman.d/mirrorlist
pacman -Syy 

# get system applications
pacstrap $MOUNT_DIR $PACSTRAP_PACKAGES

# arch mirror cache -> destination system
if [[ -n "$arch_mirror" ]]; then
    echo "Server = http://$arch_mirror/archlinux/\$repo/os/\$arch" > $MOUNT_DIR/etc/pacman.d/archmirror

    sed 's:\[core\]:\[core\]\nInclude = /etc/pacman.d/archmirror:g' -i $MOUNT_DIR/etc/pacman.conf
    sed 's:\[extra\]:\[extra\]\nInclude = /etc/pacman.d/archmirror:g' -i $MOUNT_DIR/etc/pacman.conf
    sed 's:\[community\]:\[community\]\nInclude = /etc/pacman.d/archmirror:g' -i $MOUNT_DIR/etc/pacman.conf
    sed 's:\[multilib\]:\[multilib\]\n#Include = /etc/pacman.d/archmirror:g' -i $MOUNT_DIR/etc/pacman.conf
fi

# prepare base system config
genfstab -Up $MOUNT_DIR > $MOUNT_DIR/etc/fstab
echo $arch_host_name > $MOUNT_DIR/etc/hostname
echo "blacklist pcspkr" > $MOUNT_DIR/etc/modprobe.d/nobeep.conf
ln -sf /usr/share/zoneinfo/Europe/Berlin $MOUNT_DIR/etc/localtime

echo "KEYMAP=de-latin1" > $MOUNT_DIR/etc/vconsole.conf
if [[ $arch_lang_de == 0 ]]; then
    echo "LANG=de_DE.UTF-8" > $MOUNT_DIR/etc/locale.conf
    echo "LANGUAGE=de_DE" >> $MOUNT_DIR/etc/locale.conf
    sed 's/#de_DE.UTF-8 UTF-8/de_DE.UTF-8 UTF-8/g' -i $MOUNT_DIR/etc/locale.gen
else
    echo "LANG=en_US.UTF-8" > $MOUNT_DIR/etc/locale.conf
    echo "LANGUAGE=en_US" >> $MOUNT_DIR/etc/locale.conf
fi
sed 's/#en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/g' -i $MOUNT_DIR/etc/locale.gen
arch-chroot $MOUNT_DIR locale-gen

# enable sudo without password
# IMPORTANT: remove after complete installation
sed 's/# %wheel ALL=(ALL) NOPASSWD: ALL/%wheel ALL=(ALL) NOPASSWD: ALL/g' -i $MOUNT_DIR/etc/sudoers

# enable pacman color
sed 's/#Color/Color/g' -i $MOUNT_DIR/etc/pacman.conf

# start base services
arch-chroot $MOUNT_DIR systemctl enable sshd
arch-chroot $MOUNT_DIR systemctl enable NetworkManager
arch-chroot $MOUNT_DIR systemctl enable acpid
arch-chroot $MOUNT_DIR systemctl enable ntpd

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# >> VIM <<
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
echo ">>Prepare VIM"
# vim fix -> remove vi + simlink
arch-chroot $MOUNT_DIR pacman -Rs --noconfirm vi || true
ln -s /usr/bin/vim $MOUNT_DIR/usr/bin/vi

# enable vim syntax
cat >>$MOUNT_DIR/etc/vimrc <<EOL
syntax on

if has("autocmd")
  au BufReadPost * if line("'\\"") > 1 && line("'\\"") <= line("\\$") | exe "normal! g'\\"" | endif
endif
EOL

# disable visual mode by default
cat >$MOUNT_DIR/etc/skel/.vimrc <<EOL
set mouse-=a

EOL
cp $MOUNT_DIR/etc/skel/.vimrc $MOUNT_DIR/root/.vimrc

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# >> User settings <<
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
echo ">>Prepare User settings"
echo "Set root password"
echo "root:$arch_root_password" | arch-chroot $MOUNT_DIR chpasswd

echo "Create user $arch_user_name"
arch-chroot $MOUNT_DIR useradd -m -g users -G wheel -s /bin/bash $arch_user_name
echo "$arch_user_name:$arch_user_password" | arch-chroot $MOUNT_DIR chpasswd

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# >> System tweaks <<
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
echo "fs.inotify.max_user_watches=524288" > $MOUNT_DIR/etc/sysctl.d/inotify.conf

# SSH agent
mkdir -p $MOUNT_DIR/home/$arch_user_name/.config/systemd/user/
cat >$MOUNT_DIR/home/$arch_user_name/.config/systemd/user/ssh-agent.service <<EOL
[Unit]
Description=SSH key agent

[Service]
Type=forking
Environment=SSH_AUTH_SOCK=%t/ssh-agent.socket
ExecStart=/usr/bin/ssh-agent -a \$SSH_AUTH_SOCK

[Install]
WantedBy=default.target
EOL

arch-chroot $MOUNT_DIR chown -R "$arch_user_name:users" /home/$arch_user_name/.config

# actual broken -> TODO find workaround
#arch-chroot $MOUNT_DIR sudo -u $arch_user_name systemctl --user enable ssh-agent
echo "# enable SSH agent" > $MOUNT_DIR/home/$arch_user_name/TODO.txt
echo "systemctl --user enable ssh-agent" >> $MOUNT_DIR/home/$arch_user_name/TODO.txt

cat >>$MOUNT_DIR/etc/skel/.bashrc <<EOL

export SSH_AUTH_SOCK="$XDG_RUNTIME_DIR/ssh-agent.socket"
EOL

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# >> Desktop <<
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

if [[ $arch_is_desktop == 0 ]]; then

    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
    # >> KDE <<
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
    echo ">>Prepare KDE"

    # set keyboard layout
    # disable visual mode by default
    cat >$MOUNT_DIR/etc/X11/xorg.conf.d/00-keyboard.conf <<EOL
Section "InputClass"
        Identifier "keyboard"
        MatchIsKeyboard "yes"
        Option "XkbLayout" "de"
        Option "XkbModel" "pc105"
        Option "XkbVariant" "nodeadkeys"
EndSection
EOL

    # copy default sddm config
    cp $MOUNT_DIR/usr/lib/sddm/sddm.conf.d/default.conf $MOUNT_DIR/etc/sddm.conf

    # improve sddm config for kde
    sed 's/Current=/Current=breeze/g' -i $MOUNT_DIR/etc/sddm.conf

    # improve pulse audio
    sed 's/load-module module-role-cork/#load-module module-role-cork/g' -i $MOUNT_DIR/etc/pulse/default.pa

    # improve tilix window
    arch-chroot $MOUNT_DIR sudo -u $arch_user_name dbus-launch --exit-with-session dconf write /com/gexperts/Tilix/window-style "'disable-csd'"

    # enable desktop manager
    arch-chroot $MOUNT_DIR systemctl enable sddm
fi

# fix Xorg for xpra
cat >$MOUNT_DIR/etc/X11/Xwrapper.config  <<EOL
allowed_users = anybody
needs_root_rights = no
EOL

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# >> AUR packages <<
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# install yay
echo ">>Install yay"

mkdir -p $MOUNT_DIR/aur/
curl https://aur.archlinux.org/cgit/aur.git/snapshot/yay.tar.gz | tar xvz -C $MOUNT_DIR/aur/
chmod -R 777 $MOUNT_DIR/aur/
arch-chroot $MOUNT_DIR bash -c "cd /aur/yay && sudo -u $arch_user_name makepkg --noconfirm -irs"
rm -rf $MOUNT_DIR/aur/

echo ">>Install AUR packages"
arch-chroot $MOUNT_DIR sudo -u $arch_user_name yay -S --nodiffmenu --nocleanmenu --noeditmenu --nouseask --noprovides $AUR_PACKAGES

# remove sudo without password and enable sudo with password
sed 's/%wheel ALL=(ALL) NOPASSWD: ALL/# %wheel ALL=(ALL) NOPASSWD: ALL/g' -i $MOUNT_DIR/etc/sudoers
sed 's/# %wheel ALL=(ALL) ALL/%wheel ALL=(ALL) ALL/g' -i $MOUNT_DIR/etc/sudoers

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# >> Load user dotfiles <<
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
if [[ -n "$user_dotfiles_repo" ]]; then
    arch-chroot $MOUNT_DIR sudo -u $arch_user_name git clone --bare $user_dotfiles_repo /home/$arch_user_name/.dotfiles/
    arch-chroot $MOUNT_DIR sudo -u $arch_user_name git --git-dir=/home/$arch_user_name/.dotfiles/ --work-tree=/home/$arch_user_name/ checkout -f
fi

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# >> Encrypt home directory of user <<
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
if [[ $encrypt_home == 0 ]]; then
    # https://wiki.gentoo.org/wiki/KDE#KWallet_auto-unlocking
    echo "# kwallet unlock on login" >> $MOUNT_DIR/home/$arch_user_name/TODO.txt
    echo "# copy ~/.local/share/kwalletd/kdewallet.salt to unencrypted location" >> $MOUNT_DIR/home/$arch_user_name/TODO.txt

    # enable ecryptfs
    modprobe ecryptfs
    echo 'ecryptfs' > $MOUNT_DIR/etc/modules-load.d/ecryptfs.conf

    # add required pam modules
    sed -E '/auth[[:space:]]+required[[:space:]]+pam_unix.so/a auth required pam_ecryptfs.so unwrap' -i $MOUNT_DIR/etc/pam.d/system-auth
    sed -E '/password[[:space:]]+required[[:space:]]+pam_unix.so/i password optional pam_ecryptfs.so' -i  $MOUNT_DIR/etc/pam.d/system-auth
    sed -E '/session[[:space:]]+required[[:space:]]+pam_unix.so/a session optional pam_ecryptfs.so unwrap' -i  $MOUNT_DIR/etc/pam.d/system-auth

    # create encrypted home directory
    export LOGINPASS="$arch_user_password"
    arch-chroot $MOUNT_DIR bash -c "ecryptfs-migrate-home -u $arch_user_name"
    export LOGINPASS=""
fi


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# >> Bootloader <<
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
echo ">>Prepare bootloader"


if [[ $arch_use_grub == 0 ]]; then
    echo "-> Grub"
    # reduce timeout
    sed 's/GRUB_TIMEOUT=5/GRUB_TIMEOUT=1/g' -i $MOUNT_DIR/etc/default/grub
    sed 's/GRUB_CMDLINE_LINUX_DEFAULT="quiet"/GRUB_CMDLINE_LINUX_DEFAULT="text"/g' -i $MOUNT_DIR/etc/default/grub

    mkdir -p $MOUNT_DIR/boot/grub/
    arch-chroot $MOUNT_DIR grub-mkconfig -o /boot/grub/grub.cfg

    echo "IMPORTANT: Install grub to boot sector \"arch-chroot $MOUNT_DIR grub-install /dev/...\""

elif [[ $arch_use_systemd == 0 ]]; then
    echo "-> Systemd"

    arch-chroot $MOUNT_DIR bootctl install

    cat >$MOUNT_DIR/boot/loader/entries/arch-uefi.conf <<EOL
title    Arch Linux
linux    /vmlinuz-linux
initrd   /initramfs-linux.img
options  root=LABEL=arch rw resume=swap
EOL

    cat >$MOUNT_DIR/boot/loader/entries/arch-uefi-fallback.conf <<EOL
title    Arch Linux Fallback
linux    /vmlinuz-linux
initrd   /initramfs-linux-fallback.img
options  root=LABEL=arch rw resume=swap
EOL

    cat >$MOUNT_DIR/boot/loader/loader.conf <<EOL
default   arch-uefi
timeout   1
EOL
fi
